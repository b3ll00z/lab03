
public class Game {
	
	private static int MAX_FRAME_SCORE = 10;
	private static int MAX_FRAMES = 24;

	
	private int[] frames;
	private int frame;
	
	public Game() {
		
		frames = new int[MAX_FRAMES];
		
		for (int i = 0; i < MAX_FRAMES; i++)
				frames[i] = 0;
		
		frame = 0;
	} 

	public void roll(int pins){
		
		frames[frame] = pins;
		
		if (pins == MAX_FRAME_SCORE)
			frame += 2;
		else
			frame++;
	}


	public int score(){
		
		int totalScore = 0;
			
		for (int session = 0; session < 20; session++)
			totalScore += frames[session];
			
		return totalScore + makePrizeScore();
	}

	private int makePrizeScore() {
		
		int prizeScore = 0;
		
		for (int launch = 2; launch < 22; launch++) {

			/* Gestione punteggio in caso di Strike */
			if (launch % 2 != 0 && frames[launch - 3] == MAX_FRAME_SCORE) {
				
				/* Accumulo premio per lo strike precedente */
				if (frames[launch] != 0) 	
					prizeScore += (frames[launch - 1] + frames[launch]);
				
				/* Ho fatto strike anche in questo frame */
				if (frames[launch - 1] == MAX_FRAME_SCORE)	
						prizeScore += MAX_FRAME_SCORE + frames[launch + 1];
				
			}
			
			/* Gestione punteggio in caso di Spare */
			if (launch % 2 == 0 && frames[launch - 1] != 0 &&	
						frames[launch - 1] + frames[launch - 2] == MAX_FRAME_SCORE)
				prizeScore += frames[launch]; 
		}			
		
		return prizeScore;
	}

}

